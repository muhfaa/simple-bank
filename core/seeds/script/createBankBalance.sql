CREATE TABLE IF NOT EXISTS bank_balance (
	id int NOT NULL AUTO_INCREMENT,
	balance int,
	balanceAchieve int,
	code varchar(100),
	enable BOOLEAN,
	userBalanceID int,
	PRIMARY KEY (id)
)
