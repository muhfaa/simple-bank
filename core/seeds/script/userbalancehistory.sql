INSERT INTO user_balance_history (
		userBalanceID,
		balanceBefore,
		balanceAfter,
		activity,
		type,
		ip,
		location,
		userAgent,
		author
	)
	VALUE
	(1, 0, 1000000, "TopUp", "debit", "0.0.0.0", "indonesia", "aldi", "aldi"),
    (2, 0, 2000000, "TopUp", "debit", "0.0.0.0", "indonesia", "akbar", "akbar"),
    (3, 0, 3000000, "TopUp", "debit", "0.0.0.0", "indonesia", "lazuardi", "lazuardi")