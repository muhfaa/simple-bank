CREATE TABLE IF NOT EXISTS user_balance (
	id int NOT NULL AUTO_INCREMENT,
	userid int,
	balance int,
	balanceAchieve int,
	PRIMARY KEY (id)
)