CREATE TABLE IF NOT EXISTS bank_balance_history (
	id int NOT NULL AUTO_INCREMENT,
	bankBalanceID int,
	balanceBefore int,
	balanceAfter int,
	activity varchar(100),
	type enum('kredit', 'debit'),
	ip varchar(100),
	location varchar(100),
	userAgent varchar(100),
	author varchar(100),
	PRIMARY KEY (id)
)