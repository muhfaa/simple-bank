package seeds

import (
	"io/ioutil"
	"path"
	"runtime"

	"github.com/bxcodec/faker/v3"
)

// CreateDB ...
func (s Seed) CreateDB() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/createDB.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// CreateUserSeed ...
func (s Seed) CreateUserSeed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/createUser.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// CreateUserBalanceSeed ...
func (s Seed) CreateUserBalanceSeed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/createUserBalance.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// CreateUserBalanceHisSeed ...
func (s Seed) CreateUserBalanceHisSeed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/createUserBalanceHis.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// CreateBankBalanceSeed ...
func (s Seed) CreateBankBalanceSeed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/createBankBalance.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// CreateBankBalanceHisSeed ...
func (s Seed) CreateBankBalanceHisSeed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/createBankBalanceHis.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// Insert
// ======================================================
// UserSeed ..
func (s Seed) UserSeed() {
	for i := 0; i < 20; i++ {
		// prepare statement
		stmt, _ := s.db.Prepare(`INSERT INTO user(username, email, password) VALUE (?, ?, ?)`)
		// execute query
		_, err := stmt.Exec(faker.Username(), faker.Email(), faker.Password())
		if err != nil {
			panic(err)
		}
	}
}

// UserBalanceSeed ...
func (s Seed) UserBalanceSeed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/userbalance.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// UserBalanceHistorySeed ...
func (s Seed) UserBalanceHistorySeed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/userbalancehistory.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// BankBalanceSeed ...
func (s Seed) BankBalanceSeed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/bankbalance.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// BankBalanceHistorySeed ...
func (s Seed) BankBalanceHistorySeed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/script/bankbalancehistory.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

// GetSourcePath ...
func GetSourcePath() string {
	_, filename, _, _ := runtime.Caller(1)
	return path.Dir(filename)
}
