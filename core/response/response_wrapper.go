package response

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// BaseResponse ...
type BaseResponse struct {
	Rescode int    `json:"rescode"`
	Message string `json:"message"`
}

// ErrResponse ...
type ErrResponse struct {
	BaseResponse
	Data interface{} `json:"data"`
}

// SetErrorHandler ...
func SetErrorHandler(e *echo.Echo) {
	e.HTTPErrorHandler = func(err error, c echo.Context) {
		if c.Response().Committed {
			return
		}

		// Default
		rescode := http.StatusInternalServerError
		message := "Unknown Error"

		errEcho, y := err.(*echo.HTTPError)
		if y {
			rescode = errEcho.Code
		}

		msgEcho, y := errEcho.Message.(string)
		if y {
			message = msgEcho
		}

		c.JSON(rescode, ErrResponse{BaseResponse{rescode, message}, map[string]string{}})
	}
}

// CommonResponseWrapper ...
func CommonResponseWrapper(rescode int, message string) ErrResponse {
	return ErrResponse{BaseResponse{rescode, message}, map[string]string{}}
}
