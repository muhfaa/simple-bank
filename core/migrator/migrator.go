package migrator

import (
	"fmt"
	"io"
	"log"
	"os/exec"
	"simple-bank/core/config"
)

// Migrate ...
func Migrate(cfg config.Config, arg string) {

	var migratePath string

	if len(config.APP_PATH) > 0 {
		migratePath = fmt.Sprintf("%s/migration/mysql/", config.APP_PATH)
	} else {
		migratePath = "migration/mysql/"
	}

	rawCMD := "echo \"y\" | migrate -database \"mysql://%s:%s@tcp(%s:%s)/%s\" -path %s %s"
	command := fmt.Sprintf(rawCMD,
		cfg.Database.Mysql.User,
		cfg.Database.Mysql.Pwd,
		cfg.Database.Mysql.Host,
		cfg.Database.Mysql.Port,
		cfg.Database.Mysql.Name,
		migratePath,
		arg)
	cmd := exec.Command("bash", "-c", command)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		defer stdin.Close()
		io.WriteString(stdin, "values written to stdin are passed to cmd's standard input")
	}()

	out, _ := cmd.CombinedOutput()
	log.Printf("%s", out)
}
