package arguments

import (
	"log"
	"simple-bank/core/config"
	"simple-bank/core/migrator"
)

func Handler(args []string, config config.Config) {
	switch args[0] {
	case "help":
		log.Print(`
		Usage:

			go run main.go <command>

		The commands are:
		help			Show helper
		migrateup		Migrate the DB to the most recent version available
		migratedown		Rolback migration, WARNING : please make sure the query in rollback / down fole, its may contains DROP TABLE, the data stored will lost
		`)
	case "migrateup":
		migrator.Migrate(config, "up")
	case "migratedown":
		migrator.Migrate(config, "down")
	}
}
