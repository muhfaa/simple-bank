package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

var (
	// APP_PATH ...
	APP_PATH = os.Getenv("APP_PATH")
)

// Config ...
type Config struct {
	Database struct {
		Mysql struct {
			Host string `json:"host"`
			Port string `json:"port"`
			User string `json:"user"`
			Pwd  string `json:"pwd"`
			Name string `json:"name"`
		} `json:"mysql"`
		Redis struct {
			Driver   string `json:"driver"`
			Host     string `json:"host"`
			Password string `json:"password"`
			DBNumber int    `json:"db_number"`
			Port     string `json:"port"`
		} `json:"redis"`
	} `json:"database"`
	JWT struct {
		SigningKey string `json:"signing_key"`
		ExpiredAt  int    `json:"expired_at"` // hour
	} `json:"jwt"`
}

// InitConfig ...
func InitConfig() Config {
	var (
		jsonByte []byte
		err      error
	)

	if len(APP_PATH) > 0 {
		jsonByte, err = ioutil.ReadFile(fmt.Sprintf("%s/config/config.json", APP_PATH))
	} else {
		jsonByte, err = ioutil.ReadFile("config/config.json")
	}

	if err != nil {
		panic(err)
	}

	var cfg Config

	err = json.Unmarshal(jsonByte, &cfg)
	if err != nil {
		panic(err)
	}

	return cfg
}
