package redis

import (
	"simple-bank/core/config"

	"github.com/go-redis/redis"
)

type CacheConnection struct {
	Redis *redis.Client
}

func NewCacheConnection(config config.Config) *CacheConnection {
	var db CacheConnection
	RedisConnect(&db, config)

	return &db
}

func RedisConnect(db *CacheConnection, config config.Config) {
	client := redis.NewClient(&redis.Options{
		Addr:     config.Database.Redis.Host + ":" + config.Database.Redis.Port,
		Password: config.Database.Redis.Password,
		DB:       config.Database.Redis.DBNumber,
	})

	db.Redis = client
}
