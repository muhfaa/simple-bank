package mysql

import (
	"context"
	"fmt"
	"simple-bank/core/config"
	"simple-bank/util/logger"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// DatabaseConnection ...
type DatabaseConnection struct {
	MySQL *sqlx.DB
}

// NewDatabaseConnection ...
func NewDatabaseConnection(config config.Config) *DatabaseConnection {
	var db DatabaseConnection
	newMysqlDB(&db, config)

	return &db
}

func newMysqlDB(db *DatabaseConnection, config config.Config) {
	uri := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		config.Database.Mysql.User,
		config.Database.Mysql.Pwd,
		config.Database.Mysql.Host,
		config.Database.Mysql.Port,
		config.Database.Mysql.Name)

	mysqlDB, err := sqlx.Open("mysql", uri)
	if err != nil {
		logger.Print(err)
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	err = mysqlDB.PingContext(ctx)
	if err != nil {
		logger.Print(err)
		panic(err)
	}

	db.MySQL = mysqlDB
}
