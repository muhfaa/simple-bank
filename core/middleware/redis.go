package middleware

import (
	"net/http"
	businessRedis "simple-bank/business/redis"
	"simple-bank/core/auth"
	"simple-bank/core/response"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
)

// CheckID is method for checking user permisson
func CheckID(cacheRepo businessRedis.RedisRepository) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			customer := auth.GetUser(c)

			token := strings.TrimPrefix(c.Request().Header.Get("Authorization"), "Bearer ")

			cacheToken := cacheRepo.Get("login:" + strconv.Itoa(customer.ID) + ":" + token)
			if customer.ID > 0 && cacheToken == token {
				return next(c)
			}

			return c.JSON(http.StatusForbidden, response.CommonResponseWrapper(http.StatusForbidden, http.StatusText(http.StatusForbidden)))
		}
	}
}
