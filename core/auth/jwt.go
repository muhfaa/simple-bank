package auth

import (
	"simple-bank/core/config"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// JwtCustomClaims ...
type JwtCustomClaims struct {
	ID int `json:"id"`
	jwt.StandardClaims
}

// JwtConfig ...
var JwtConfig middleware.JWTConfig

func init() {
	config := config.InitConfig()
	JwtConfig = middleware.JWTConfig{
		Claims:     &JwtCustomClaims{},
		SigningKey: []byte(config.JWT.SigningKey),
	}
}

// GenerateToken jwt ...
func GenerateToken(userID int) (token string) {
	config := config.InitConfig()
	claims := JwtCustomClaims{
		userID,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Local().Add(time.Hour * time.Duration(int64(config.JWT.ExpiredAt))).Unix(),
		},
	}

	// Create token with claims
	t := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, _ = t.SignedString([]byte(config.JWT.SigningKey))

	return
}

// GetUser from jwt ...
func GetUser(c echo.Context) *JwtCustomClaims {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*JwtCustomClaims)
	return claims
}
