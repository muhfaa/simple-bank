package logger

import (
	"log"
	"os"
	"time"
)

var (
	// LOGFORMAT ...
	LOGFORMAT = `{"time":"${time_rfc3339_nano}","level":"${level}","prefix":"${prefix}","log":%s}`

	// FILE ...
	FILE = "file"
	// LINE ...
	LINE = "line"
	// PRNT ...
	PRNT = "print"

	// MESSAGE ...
	MESSAGE = "message"

	// LEVEL value should be info/error...
	LEVEL = "level"
	// INFO ...
	INFO = "info"
	// ERROR ...
	ERROR = "error"
)

func logFile() (f *os.File) {
	f, err := os.OpenFile("./logs/app."+time.Now().Format("2006010215")+".log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	return
}
