package logger

import (
	"encoding/json"
	"fmt"
	"io"
	"runtime"

	"github.com/labstack/gommon/log"
)

// Print ...
func Print(message ...interface{}) {
	thisLog := make(map[string]interface{})
	_, file, line, _ := runtime.Caller(1)
	thisLog[FILE] = file
	thisLog[LINE] = line
	thisLog[PRNT] = fmt.Sprint(message...)
	jsonLog, _ := json.Marshal(thisLog)
	log.SetPrefix("print_log")
	log.SetHeader(fmt.Sprintf(LOGFORMAT, string(jsonLog)))
	log.Print()
}

// Log ...
func Log(thisLog map[string]interface{}, message ...string) {
	_, file, line, _ := runtime.Caller(1)
	thisLog[FILE] = file
	thisLog[LINE] = line
	jsonLog, _ := json.Marshal(thisLog)
	f := logFile()
	defer f.Close()
	write := io.Writer(f)
	log.SetPrefix("custom")
	log.SetOutput(write)
	log.SetHeader(fmt.Sprintf(LOGFORMAT, string(jsonLog)))
	if thisLog[LEVEL] == ERROR {
		log.Error(message)
	} else {
		log.Info(message)
	}
}
