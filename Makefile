GOCMD = go
GOBUILD = $(GOCMD) build
GOMOD = $(GOCMD) mod
GOTEST = $(GOCMD) test
BINARY_NAME = simpeBankAPI

all: serve

init:
	$(GOMOD) init $(module)

install:
	$(GOMOD) tidy

serve:
	$(GOCMD) run .

hotreload:
	$ reflex -r '\.go' -s -- sh -c 'make serve'

build:
	# ----- linux env
	# CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) -o ./$(BINARY_NAME) -v ./
	# ----- osx env
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 $(GOBUILD) -o ./$(BINARY_NAME) -v ./
	# ----- windows env
	# CGO_ENABLED=0 GOOS=windows GOARCH=amd64 $(GOBUILD) -o ./$(BINARY_NAME) -v ./

app:
	# make app arg=help | make app arg=migrateup | make app arg=migratedown
ifeq ($(arg),)
	$(GOCMD) run main.go help
else
	$(GOCMD) run main.go $(arg)
endif

.PHONY: all serve build
