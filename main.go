package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	srvcUser "simple-bank/business/user"
	"simple-bank/core/arguments"
	"simple-bank/core/config"
	"simple-bank/core/database/mysql"
	"simple-bank/core/database/redis"
	ctrlUser "simple-bank/modules/api/user"
	"time"

	repo "simple-bank/modules/repository/factory"

	api "simple-bank/modules/api/router"

	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/redhajuanda/goseeder/seeds"
)

func main() {
	// Load Config
	config := config.InitConfig()

	if args := os.Args; len(args) > 1 {
		arguments.Handler(args[1:], config)
	}

	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.RemoveTrailingSlash())
	e.Use(ServiceRequestTime)

	e.GET("/", func(c echo.Context) error {
		message := `Welcome to Simple Bank`
		return c.String(http.StatusOK, message)
	})

	// Create seeder
	godotenv.Load()
	handleArgs(config)

	dbCon := mysql.NewDatabaseConnection(config)
	cacheCon := redis.NewCacheConnection(config)

	cacheRepo := repo.NewCacheFactory(cacheCon)

	userRepo := repo.NewUserMySQLFactory(dbCon)
	userService := srvcUser.NewUserService(userRepo, cacheRepo)
	userController := ctrlUser.NewUserController(userService)

	api.Route(e, api.TheController{
		UserCtrl:  userController,
		CacheRepo: cacheRepo,
	})

	// Start server
	e.Logger.Fatal(e.Start(":7070"))
}

func ServiceRequestTime(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Request().Header.Set("X-Service-RequestTime", time.Now().Format(time.RFC3339))
		return next(c)
	}
}

func handleArgs(config config.Config) {
	flag.Parse()
	args := flag.Args()

	if len(args) >= 1 {
		switch args[0] {
		case "seed":
			uri := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
				config.Database.Mysql.User,
				config.Database.Mysql.Pwd,
				config.Database.Mysql.Host,
				config.Database.Mysql.Port,
				config.Database.Mysql.Name)
			// connect DB
			db, err := sqlx.Open("mysql", uri)
			if err != nil {
				log.Fatalf("Error opening DB: %v", err)
			}
			seeds.Execute(db, args[1:]...)
			os.Exit(0)
		}
	}
}
