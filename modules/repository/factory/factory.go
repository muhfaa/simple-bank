package factory

import (
	business "simple-bank/business/user"
	databaseMySQL "simple-bank/core/database/mysql"
	repositoryMySQL "simple-bank/modules/repository/mysql"

	businessRedis "simple-bank/business/redis"
	cacheRedis "simple-bank/core/database/redis"
	repositoryRedis "simple-bank/modules/repository/cache"
)

// NewUserMySQLFactory ...
func NewUserMySQLFactory(dbCon *databaseMySQL.DatabaseConnection) (repo business.UserRepository) {
	repo = repositoryMySQL.NewMySQLRepository(dbCon.MySQL)
	return
}

// NewCacheFactory ....
func NewCacheFactory(cacheCon *cacheRedis.CacheConnection) (repo businessRedis.RedisRepository) {
	repo = repositoryRedis.NewCacheRepository(cacheCon.Redis)
	return
}
