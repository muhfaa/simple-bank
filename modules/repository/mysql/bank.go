package repository

import (
	"database/sql"
	spec "simple-bank/business/spec"
	"simple-bank/util/logger"
)

// AddBankBalance ...
func (repo *MySQLRepository) AddBankBalance(con *spec.BankBalance) (bankBalanceID int, err error) {
	tx, err := repo.db.Begin()
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	selectQuery := `INSERT INTO bank_balance(balance, balanceAchieve, code, enable, userBalanceID) VALUE (?, ?, ?, ?, ?)`

	res, err := tx.Exec(selectQuery, con.Balance, con.BalanceAchieve, con.Code, con.Enable, con.UserBalanceID)
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		tx.Rollback()
		err = spec.ErrResource
		return
	}

	if err = tx.Commit(); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	lID, _ := res.LastInsertId()
	bankBalanceID = int(lID)

	return
}

// AddBankBalanceHistory ...
func (repo *MySQLRepository) AddBankBalanceHistory(con *spec.BankBalanceHistory) (err error) {
	tx, err := repo.db.Begin()
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	selectQuery := `INSERT INTO bank_balance_history(
		bankBalanceID,
		balanceBefore,
		balanceAfter,
		activity,
		type,
		ip,
		location,
		userAgent,
		author
	)
	VALUE
	(?, ?, ?, ?, ?, ?, ?, ?, ?);
	`
	_, err = tx.Exec(selectQuery, con.BankBalanceID, con.BalanceBefore, con.BalanceAfter, con.Activity, con.Type, con.IP, con.Location, con.UserAgent, con.Author)
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		tx.Rollback()
		err = spec.ErrResource
		return
	}

	if err = tx.Commit(); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	return
}

// UpdateBankBalance ...
func (repo *MySQLRepository) UpdateBankBalance(con *spec.BankBalance) (err error) {
	tx, err := repo.db.Begin()
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	selectQuery := `UPDATE bank_balance
	SET 
	balance = ?, 
	balanceAchieve = ?, 
	code = ?, 
	enable = ?
	WHERE 
	userBalanceID = ?
	`
	_, err = tx.Exec(selectQuery, con.Balance, con.BalanceAchieve, con.Code, con.Enable, con.UserBalanceID)
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		tx.Rollback()
		err = spec.ErrResource
		return
	}

	if err = tx.Commit(); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	return
}

// SelecBankBalanceByID ...
func (repo *MySQLRepository) SelecBankBalanceByID(id int) (bankBalance spec.BankBalance, err error) {
	selectQuery := `SELECT * FROM bank_balance WHERE userBalanceID=?`

	err = repo.db.
		QueryRowx(selectQuery, id).
		StructScan(&bankBalance)

	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		if err == sql.ErrNoRows {
			err = spec.ErrUserrNotFound
			return
		}
		err = spec.ErrResource
		return
	}
	return
}

// SelectBankBalanceHistoryByID ...
func (repo *MySQLRepository) SelectBankBalanceHistoryByID(id int) (bankBalanceHistory spec.BankBalanceHistory, err error) {
	selectQuery := `SELECT * FROM bank_balance_history WHERE bankBalanceID=?`

	err = repo.db.
		QueryRowx(selectQuery, id).
		StructScan(&bankBalanceHistory)

	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		if err == sql.ErrNoRows {
			err = spec.ErrUserrNotFound
			return
		}
		err = spec.ErrResource
		return
	}
	return
}
