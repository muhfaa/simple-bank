package repository

import (
	"database/sql"
	spec "simple-bank/business/spec"
	"simple-bank/util/logger"
	"strings"
)

// InsertUser ...
func (repo *MySQLRepository) InsertUser(usr spec.UserRegister) (userID int, err error) {
	tx, err := repo.db.Begin()
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	insertQuery := `INSERT INTO user (
		username,
		email,
		password
	)
	VALUES
	(?, ?, ?)`

	res, err := tx.Exec(insertQuery, usr.Username, usr.Email, usr.Password)
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		tx.Rollback()
		if strings.Contains(err.Error(), "Duplicate entry") {
			err = spec.ErrDuplicateEntryEmailOrUsername
			return
		}
		err = spec.ErrResource
		return
	}

	if err = tx.Commit(); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	lID, _ := res.LastInsertId()
	userID = int(lID)

	return
}

// SelectUserByUsername ...
func (repo *MySQLRepository) SelectUserByUsername(username string) (user spec.User, err error) {
	selectQuery := `SELECT * FROM user WHERE username=?`

	err = repo.db.
		QueryRowx(selectQuery, username).
		StructScan(&user)

	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		if err == sql.ErrNoRows {
			err = spec.ErrUserrNotFound
			return
		}
		err = spec.ErrResource
		return
	}
	return
}

// SelectUserByID ...
func (repo *MySQLRepository) SelectUserByID(id int) (user spec.User, err error) {
	selectQuery := `SELECT * FROM user WHERE id=?`

	err = repo.db.
		QueryRowx(selectQuery, id).
		StructScan(&user)

	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		if err == sql.ErrNoRows {
			err = spec.ErrUserrNotFound
			return
		}
		err = spec.ErrResource
		return
	}
	return
}

// UpdateUserBalance ...
func (repo *MySQLRepository) UpdateUserBalance(con *spec.UserBalance) (err error) {
	tx, err := repo.db.Begin()
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	selectQuery := `UPDATE user_balance
	SET 
	balance = ?, 
	balanceAchieve = ?
	WHERE 
	userid = ?
	`

	_, err = tx.Exec(selectQuery, con.Balance, con.BalanceAchieve, con.UserID)

	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		tx.Rollback()
		err = spec.ErrResource
		return
	}

	if err = tx.Commit(); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	return
}

// AddUserBalance ...
func (repo *MySQLRepository) AddUserBalance(con *spec.UserBalance) (userBalanceID int, err error) {
	tx, err := repo.db.Begin()
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	selectQuery := `INSERT INTO user_balance (
		userid,
		balance,
		balanceAchieve)

		VALUE
		(?, ?, ?)
		`

	res, err := tx.Exec(selectQuery, con.UserID, con.Balance, con.BalanceAchieve)
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		tx.Rollback()
		err = spec.ErrResource
		return
	}

	if err = tx.Commit(); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	lID, _ := res.LastInsertId()
	userBalanceID = int(lID)

	return
}

// AddUserBalanceHistory ...
func (repo *MySQLRepository) AddUserBalanceHistory(con *spec.UserBalanceHistory) (err error) {
	tx, err := repo.db.Begin()
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}

	selectQuery := `INSERT INTO user_balance_history (
		userBalanceID,
		balanceBefore,
		balanceAfter,
		activity,
		type,
		ip,
		location,
		userAgent,
		author
	)
	VALUE
	(?, ?, ?, ?, ?, ?, ?, ?, ?);
	`
	_, err = tx.Exec(selectQuery, con.UserBalanceID, con.BalanceBefore, con.BalanceAfter, con.Activity, con.Type, con.IP, con.Location, con.UserAgent, con.Author)
	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		tx.Rollback()
		err = spec.ErrResource
		return
	}

	if err = tx.Commit(); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrResource
		return
	}
	return
}

// SelecUserBalanceByID ...
func (repo *MySQLRepository) SelecUserBalanceByID(id int) (userBalance spec.UserBalance, err error) {
	selectQuery := `SELECT * FROM user_balance WHERE userid=?`

	err = repo.db.
		QueryRowx(selectQuery, id).
		StructScan(&userBalance)

	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		if err == sql.ErrNoRows {
			err = spec.ErrUserrNotFound
			return
		}
		err = spec.ErrResource
		return
	}
	return
}

// SelectUserBalanceHistoryByID ...
func (repo *MySQLRepository) SelectUserBalanceHistoryByID(id int) (usrBalanceHistory spec.UserBalanceHistory, err error) {
	selectQuery := `SELECT * FROM user_balance_history WHERE userBalanceID=?`

	err = repo.db.
		QueryRowx(selectQuery, id).
		StructScan(&usrBalanceHistory)

	if err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		if err == sql.ErrNoRows {
			err = spec.ErrUserrNotFound
			return
		}
		err = spec.ErrResource
		return
	}
	return
}
