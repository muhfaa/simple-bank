package repository

import (
	"github.com/jmoiron/sqlx"
)

// MySQLRepository ...
type MySQLRepository struct {
	db *sqlx.DB
}

// NewMySQLRepository ...
func NewMySQLRepository(db *sqlx.DB) *MySQLRepository {
	return &MySQLRepository{
		db,
	}
}
