package repository

import (
	"github.com/go-redis/redis"
)

// RedisRepository ...
type RedisRepository struct {
	Redis *redis.Client
}

// NewCacheRepository ...
func NewCacheRepository(Redis *redis.Client) *RedisRepository {
	return &RedisRepository{
		Redis,
	}
}
