package repository

import (
	"time"
)

// SetKey ...
func (client *RedisRepository) SetKey(key string, value interface{}, expired int64) bool {
	exp := time.Duration(expired) * time.Second
	client.Redis.Set(key, value, exp)
	return true
}

// DelKeys ...
func (client *RedisRepository) DelKeys(pattern string) bool {
	keys, err := client.Redis.Keys(pattern).Result()
	if err != nil {
		return false
	}
	client.Redis.Del(keys...)
	return true
}

// Get ...
func (client *RedisRepository) Get(key string) string {
	val, err := client.Redis.Get(key).Result()
	if err != nil {
		return ""
	}
	return val
}
