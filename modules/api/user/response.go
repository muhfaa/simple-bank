package user

import (
	"simple-bank/core/response"
)

// TokenResponse ...
type TokenResponse struct {
	response.BaseResponse
	Data struct {
		Token   string `json:"token"`
		NewUser bool   `json:"new_user"`
	} `json:"data"`
}
