package user

import (
	"net/http"
	"simple-bank/business/spec"
	business "simple-bank/business/user"
	"simple-bank/core/auth"
	"simple-bank/util/logger"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

type Controller struct {
	service   business.UserService
	validator *validator.Validate
}

func NewUserController(service business.UserService) *Controller {
	return &Controller{
		service,
		validator.New(),
	}
}

func (controller *Controller) RegisterUser(c echo.Context) error {
	userRegis := new(spec.UserRegister)
	if err := c.Bind(userRegis); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		return c.JSON(http.StatusBadRequest, map[string]string{spec.MESSAGE: err.Error()})
	}

	if err := controller.service.RegisterUser(*userRegis); err != nil {
		return c.JSON(spec.ErrToHTTPCode(err), map[string]string{spec.MESSAGE: err.Error()})
	}

	return c.JSON(http.StatusCreated, map[string]string{spec.MESSAGE: "ok"})
}

func (controller *Controller) LoginUser(c echo.Context) error {
	usrLogin := new(spec.UserLogin)
	if err := c.Bind(usrLogin); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		return c.JSON(http.StatusBadRequest, map[string]string{spec.MESSAGE: err.Error()})
	}
	token, err := controller.service.LoginUser(*usrLogin)
	if err != nil {
		return c.JSON(spec.ErrToHTTPCode(err), map[string]string{spec.MESSAGE: err.Error()})
	}

	return c.JSON(http.StatusCreated, map[string]string{spec.TOKEN: token})
}

func (controller *Controller) LogoutUser(c echo.Context) error {
	return nil
}

func (controller *Controller) TopUpBalance(c echo.Context) error {
	id := auth.GetUser(c).ID
	topUp := new(spec.TopUp)
	if err := c.Bind(topUp); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		return c.JSON(http.StatusBadRequest, map[string]string{spec.MESSAGE: err.Error()})
	}

	if err := controller.service.TopUpUserBalance(id, topUp); err != nil {
		return c.JSON(spec.ErrToHTTPCode(err), map[string]string{spec.MESSAGE: err.Error()})
	}

	return c.JSON(http.StatusCreated, map[string]string{spec.MESSAGE: "success"})
}

func (controller *Controller) Transfer(c echo.Context) error {
	id := auth.GetUser(c).ID
	transfer := new(spec.Transfer)
	if err := c.Bind(transfer); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		return c.JSON(http.StatusBadRequest, map[string]string{spec.MESSAGE: err.Error()})
	}

	if err := controller.service.Transfer(id, transfer); err != nil {
		return c.JSON(spec.ErrToHTTPCode(err), map[string]string{spec.MESSAGE: err.Error()})
	}

	return c.JSON(http.StatusCreated, map[string]string{spec.MESSAGE: "success"})
}
