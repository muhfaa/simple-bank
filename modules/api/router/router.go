package api

import (
	businessRedis "simple-bank/business/redis"
	"simple-bank/core/auth"
	simpleebankMiddleware "simple-bank/core/middleware"
	ctrlUser "simple-bank/modules/api/user"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var basePath = "api/v1"

// TheController ...
type TheController struct {
	UserCtrl  *ctrlUser.Controller
	CacheRepo businessRedis.RedisRepository
}

// Route ...
func Route(e *echo.Echo, ctrl TheController) {
	user := e.Group(basePath + "/user")
	// User
	user.POST("/register", ctrl.UserCtrl.RegisterUser)
	user.POST("/login", ctrl.UserCtrl.LoginUser)
	user.POST("/logout", ctrl.UserCtrl.LogoutUser, middleware.JWTWithConfig(auth.JwtConfig), simpleebankMiddleware.CheckID(ctrl.CacheRepo))
	user.POST("/topup", ctrl.UserCtrl.TopUpBalance, middleware.JWTWithConfig(auth.JwtConfig), simpleebankMiddleware.CheckID(ctrl.CacheRepo))
	user.POST("/transfer", ctrl.UserCtrl.Transfer, middleware.JWTWithConfig(auth.JwtConfig), simpleebankMiddleware.CheckID(ctrl.CacheRepo))

}
