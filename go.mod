module simple-bank

go 1.15

require (
	github.com/bxcodec/faker/v3 v3.5.0
	github.com/davecgh/go-spew v1.1.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/labstack/gommon v0.3.0
	github.com/redhajuanda/goseeder v0.0.0-20200920125952-5a3f22c4dad3
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
)
