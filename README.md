# Simple Bank Project

Simple Bank is a small project where users can register, login, topup and transfer between users within the same bank.

## API
Simple bank was built using hexagonal architecture. Where this architecture is included in the clean architecture, which separates the request, process, and response which is ultimately returned to the user. This project use Echo Framework to because Echo router adopts the radix tree concept, making its lookup performance so fast. Not also that, the use of sync pool makes memory usage more efficient, and safer from GC overhead.

## Pre Requirements
- $ brew install go
- $ go get github.com/golang-migrate/migrate
- $ go get github.com/cespare/reflex
- create db simple_bank (refer to config file)
- $ make install
- $ make app arg=migrateup
- $ make hotreload
- open localhost:7070 in your web browser

## Insomnia

Import file simple_bank.json to insomnia
