package user

import (
	redis "simple-bank/business/redis"
	spec "simple-bank/business/spec"
	"simple-bank/core/auth"
	"simple-bank/core/config"
	"simple-bank/util/logger"
	"simple-bank/util/password"
	"strconv"

	"github.com/go-playground/validator/v10"
)

// UserService ...
type UserService interface {
	// RegisterUser ...
	RegisterUser(user spec.UserRegister) (err error)
	// LoginUser ...
	LoginUser(user spec.UserLogin) (token string, err error)
	// LogoutUser ...
	LogoutUser(id int, token string) (err error)
	// TopupBalance ...
	TopUpUserBalance(id int, con *spec.TopUp) (err error)
	// Transfer ...
	Transfer(id int, con *spec.Transfer) (err error)
}

// UserRepository ...
type UserRepository interface {
	// InsertUser ...
	InsertUser(user spec.UserRegister) (userID int, err error)

	// SelectUserByUsername ...
	SelectUserByUsername(username string) (user spec.User, err error)

	// SelectUserByID ...
	SelectUserByID(id int) (user spec.User, err error)

	// UpdateUserBalance ...
	UpdateUserBalance(con *spec.UserBalance) (err error)

	// AddUserBalance ...
	AddUserBalance(con *spec.UserBalance) (userBalanceID int, err error)

	// AddUserBalanceHistory ...
	AddUserBalanceHistory(con *spec.UserBalanceHistory) (err error)

	// SelecUserBalanceByID ...
	SelecUserBalanceByID(id int) (userBalance spec.UserBalance, err error)

	// SelectUserBalanceHistoryByID ...
	SelectUserBalanceHistoryByID(id int) (usrBalanceHistory spec.UserBalanceHistory, err error)

	// UpdateBankBalance ...
	UpdateBankBalance(con *spec.BankBalance) (err error)

	// AddBankBalance ...
	AddBankBalance(con *spec.BankBalance) (bankBalanceID int, err error)

	// AddBankBalanceHistory ...
	AddBankBalanceHistory(con *spec.BankBalanceHistory) (err error)

	// SelecBankBalanceByID ...
	SelecBankBalanceByID(id int) (bankBalance spec.BankBalance, err error)

	// SelectBankBalanceHistoryByID ...
	SelectBankBalanceHistoryByID(id int) (bankBalanceHistory spec.BankBalanceHistory, err error)
}

type userService struct {
	userRepo  UserRepository
	redisRepo redis.RedisRepository
	validate  *validator.Validate
}

// NewUserService ...
func NewUserService(userRepo UserRepository, redisRepo redis.RedisRepository) (service UserService) {
	return &userService{
		userRepo,
		redisRepo,
		validator.New(),
	}
}

func (s *userService) RegisterUser(user spec.UserRegister) (err error) {
	if err = s.validate.Struct(user); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrInvalidSpec
		return
	}

	usr, _ := s.userRepo.SelectUserByUsername(user.Username)
	if usr.Username == user.Username {
		err = spec.ErrDuplicateEntryEmailOrUsername
		return
	}

	encPass, _ := password.HashPassword(user.Password)
	user.Password = encPass
	id, err := s.userRepo.InsertUser(user)
	if err != nil {
		err = spec.ErrResource
		return
	}

	UB := new(spec.UserBalance)
	UBH := new(spec.UserBalanceHistory)
	UB.UserID = id
	userBalanceID, err := s.userRepo.AddUserBalance(UB)
	if err != nil {
		err = spec.ErrResource
		return
	}

	UBH.UserBalanceID = userBalanceID
	UBH.IP = "0.0.0.0"
	UBH.Location = "indonesia"
	UBH.Type = "debit"
	UBH.UserAgent = user.Username
	UBH.Author = user.Username
	err = s.userRepo.AddUserBalanceHistory(UBH)
	if err != nil {
		err = spec.ErrResource
		return
	}

	BB := new(spec.BankBalance)
	BBH := new(spec.BankBalanceHistory)

	BB.UserBalanceID = userBalanceID
	bankBalanceID, err := s.userRepo.AddBankBalance(BB)
	if err != nil {
		err = spec.ErrResource
		return
	}

	BBH.BankBalanceID = bankBalanceID
	BBH.IP = "0.0.0.0"
	BBH.Location = "indonesia"
	BBH.Type = "debit"
	BBH.UserAgent = user.Username
	BBH.Author = user.Username
	err = s.userRepo.AddBankBalanceHistory(BBH)
	if err != nil {
		err = spec.ErrResource
	}

	return
}

func (s *userService) LoginUser(userLogin spec.UserLogin) (token string, err error) {
	if err = s.validate.Struct(userLogin); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrInvalidSpec
		return
	}

	user, err := s.userRepo.SelectUserByUsername(userLogin.Username)
	if err != nil {
		if err == spec.ErrUserrNotFound {
			err = spec.ErrInvalidUsernamePassword
			return
		}
	}

	if password.CheckPasswordHash(userLogin.Password, user.Password) {
		token = auth.GenerateToken(user.ID)
		config := config.InitConfig()
		s.redisRepo.SetKey("login:"+strconv.Itoa(user.ID)+":"+token, token, int64(config.JWT.ExpiredAt*60))
		return
	}

	err = spec.ErrInvalidUsernamePassword
	return
}

func (s *userService) LogoutUser(id int, token string) (err error) {

	// delete key in redis from id user
	isTrue := s.redisRepo.DelKeys("login:" + strconv.Itoa(id) + ":" + token)
	if !isTrue {
		err = spec.ErrResource
		return
	}
	return
}

func (s *userService) TopUpUserBalance(id int, con *spec.TopUp) (err error) {
	if err = s.validate.Struct(con); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrInvalidSpec
		return
	}

	// GetUser
	user, err := s.userRepo.SelectUserByID(id)
	if err != nil {
		err = spec.ErrUserrNotFound
		return
	}

	// Check userBalance
	usrBal, err := s.userRepo.SelecUserBalanceByID(id)
	if err != nil {
		err = spec.ErrUserBalanceNotFound
		return
	}

	UB := new(spec.UserBalance)
	UBH := new(spec.UserBalanceHistory)

	UB.BalanceAchieve = con.Balance
	UBH.BalanceBefore = 0
	if usrBal.Balance != 0 {
		UB.BalanceAchieve = usrBal.BalanceAchieve + con.Balance
		UBH.BalanceBefore = usrBal.Balance
	}
	UB.Balance = usrBal.BalanceAchieve
	UB.UserID = id

	// UpdateUserBalance
	err = s.userRepo.UpdateUserBalance(UB)
	if err != nil {
		if err == spec.ErrUserrNotFound {
			err = spec.ErrTopUpFail
			return
		}
	}

	UBH.UserBalanceID = UB.ID
	UBH.BalanceAfter = UB.BalanceAchieve
	UBH.Activity = "Topup"
	UBH.Type = "debit"
	UBH.IP = "0.0.0.0" //hardcode
	UBH.Location = "Indonesia"
	UBH.UserAgent = user.Username
	UBH.Author = user.Username

	// AddBalanceHistory
	err = s.userRepo.AddUserBalanceHistory(UBH)
	if err != nil {
		if err == spec.ErrUserrNotFound {
			err = spec.ErrTopUpFail
			return
		}
	}
	return
}

func (s *userService) Transfer(id int, con *spec.Transfer) (err error) {
	if err = s.validate.Struct(con); err != nil {
		logger.Log(map[string]interface{}{logger.MESSAGE: err.Error()})
		err = spec.ErrInvalidSpec
		return
	}

	UB := new(spec.UserBalance)
	UBH := new(spec.UserBalanceHistory)

	// GetUser A
	usrA, err := s.userRepo.SelectUserByID(id)
	if err != nil {
		err = spec.ErrUserrNotFound
		return
	}

	// Check userBalance
	usrBal, err := s.userRepo.SelecUserBalanceByID(id)
	if err != nil {
		err = spec.ErrUserBalanceNotFound
		return
	}

	if usrBal.BalanceAchieve == 0 {
		err = spec.ErrBalanceNotEnough
		return
	}

	if usrBal.BalanceAchieve < con.Balance {
		err = spec.ErrBalanceNotEnough
		return
	}
	// Check USerBalanceHistory
	usrBalHis, err := s.userRepo.SelectUserBalanceHistoryByID(usrBal.ID)
	if err != nil {
		err = spec.ErrUserBalanceHistoryNotFound
		return
	}

	// insert to struct userbalance account A
	UB.ID = usrBal.ID
	UB.Balance = usrBal.Balance
	if usrBal.BalanceAchieve < usrBal.Balance {
		UB.Balance = usrBalHis.BalanceAfter
	}
	UB.BalanceAchieve = usrBal.BalanceAchieve - con.Balance
	UB.UserID = id

	// UpdateUserBalance
	err = s.userRepo.UpdateUserBalance(UB)
	if err != nil {
		err = spec.ErrTransactionFail
		return
	}

	UBH.UserBalanceID = UB.ID
	UBH.BalanceBefore = UB.Balance
	UBH.BalanceAfter = UB.BalanceAchieve
	UBH.Activity = "Transfer"
	UBH.Type = "kredit"
	UBH.IP = "0.0.0.0"         //hardcode
	UBH.Location = "Indonesia" //hardcode
	UBH.UserAgent = usrA.Username
	UBH.Author = usrA.Username

	// AddBalanceHistory
	err = s.userRepo.AddUserBalanceHistory(UBH)
	if err != nil {
		err = spec.ErrTransactionFail
		return
	}

	// =======================================================================
	// update in bank_balance
	BB := new(spec.BankBalance)
	BBH := new(spec.BankBalanceHistory)

	bankBal, err := s.userRepo.SelecBankBalanceByID(UB.ID)
	if err != nil {
		err = spec.ErrBankBalanceNotFound
		return
	}

	bankBalHis, err := s.userRepo.SelectBankBalanceHistoryByID(bankBal.ID)
	if err != nil {
		err = spec.ErrBankBalanceHistoryNotFound
		return
	}
	BB.ID = bankBal.ID
	BB.Balance = bankBal.Balance
	BB.BalanceAchieve = bankBal.BalanceAchieve
	BB.Code = "0001"
	BB.Enable = true
	BB.UserBalanceID = usrBal.ID

	err = s.userRepo.UpdateBankBalance(BB)
	if err != nil {
		err = spec.ErrTransactionFail
		return
	}

	// add in bank_balance_history
	BBH.ID = bankBalHis.ID
	BBH.BankBalanceID = BB.ID
	BBH.BalanceBefore = BB.Balance
	BBH.BalanceAfter = BB.BalanceAchieve
	BBH.Activity = "Transfer"
	BBH.Type = "kredit"
	BBH.IP = "0.0.0.0" //hardcode
	BBH.Location = "Indonesia"
	BBH.UserAgent = usrA.Username
	BBH.Author = usrA.Username

	err = s.userRepo.AddBankBalanceHistory(BBH)
	if err != nil {
		err = spec.ErrTransactionFail
		return
	}

	//=============================================================================================//

	UBb := new(spec.UserBalance)
	UBHb := new(spec.UserBalanceHistory)

	// GetUser B
	usrB, err := s.userRepo.SelectUserByID(con.UserID)
	if err != nil {
		err = spec.ErrUserrNotFound
		return
	}

	// Check userBalance
	usrBalB, err := s.userRepo.SelecUserBalanceByID(id)
	if err != nil {
		err = spec.ErrUserBalanceNotFound
		return
	}
	// Check USerBalanceHistory
	usrBalHisB, err := s.userRepo.SelectUserBalanceHistoryByID(usrBal.ID)
	if err != nil {
		err = spec.ErrUserBalanceHistoryNotFound
		return
	}

	// insert to struct userbalance account B
	UBb.ID = usrBalB.ID
	UBb.Balance = usrBalB.Balance
	if usrBalB.BalanceAchieve < usrBalB.Balance {
		UBb.Balance = usrBalHisB.BalanceAfter
	}
	UBb.BalanceAchieve = UBb.BalanceAchieve + con.Balance
	UBb.UserID = con.UserID

	// UpdateUserBalance
	err = s.userRepo.UpdateUserBalance(UBb)
	if err != nil {
		err = spec.ErrTransactionFail
		return
	}

	UBHb.ID = usrBalHisB.ID
	UBHb.UserBalanceID = UBb.ID
	UBHb.BalanceBefore = UBb.Balance
	UBHb.BalanceAfter = UBb.BalanceAchieve
	UBHb.Activity = "Transfer"
	UBHb.Type = "debit"
	UBHb.IP = "0.0.0.0" //hardcode
	UBHb.Location = "Indonesia"
	UBHb.UserAgent = usrB.Username
	UBHb.Author = usrB.Username

	// AddBalanceHistory
	err = s.userRepo.AddUserBalanceHistory(UBHb)
	if err != nil {
		err = spec.ErrTransactionFail
		return
	}

	// Bank
	// =================================================================================
	// add in bank_balance_history

	BBH.BankBalanceID = BB.ID
	BBH.BalanceBefore = BB.Balance
	BBH.BalanceAfter = BB.BalanceAchieve
	BBH.Activity = "Transfer"
	BBH.Type = "debit"
	BBH.IP = "0.0.0.0" //hardcode
	BBH.Location = "Indonesia"
	BBH.UserAgent = usrB.Username
	BBH.Author = usrB.Username

	err = s.userRepo.AddBankBalanceHistory(BBH)
	if err != nil {
		err = spec.ErrTransactionFail
		return
	}

	return
}
