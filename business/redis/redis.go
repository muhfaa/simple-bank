package business

// RedisRepository ...
type RedisRepository interface {
	// SetKey
	SetKey(key string, value interface{}, expired int64) bool
	// DelKey
	DelKeys(pattern string) bool
	// Get
	Get(key string) string
}
