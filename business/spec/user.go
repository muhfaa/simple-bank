package spec

// User ...
type User struct {
	ID       int    `json:"_" db:"id"`
	Username string `json:"username" validate:"required" db:"username"`
	Email    string `json:"email" validate:"required" db:"email"`
	Password string `json:"password" validate:"required, max=20" db:"password"`
}

// UserRegister ...
type UserRegister struct {
	ID       int    `json:"_" db:"id"`
	Username string `json:"username" validate:"required" db:"username"`
	Email    string `json:"email" validate:"required" db:"email"`
	Password string `json:"password" validate:"required" db:"password"`
}
// UserLogin ...
type UserLogin struct {
	Username string `json:"username" validate:"required" db:"username"`
	Password string `json:"password" validate:"required" db:"password"`
}

// UserBalance ...
type UserBalance struct {
	ID             int `json:"_" db:"id"`
	UserID         int `json:"userid" db:"userid"`
	Balance        int `json:"balance" db:"balance"`
	BalanceAchieve int `json:"balanceAchieve" db:"balanceAchieve"`
}

// UserBalanceHistory ...
type UserBalanceHistory struct {
	ID            int    `json:"_" db:"id"`
	UserBalanceID int    `json:"userBalanceID" db:"userBalanceID"`
	BalanceBefore int    `json:"balanceBefore" db:"balanceBefore"`
	BalanceAfter  int    `json:"balanceAfter" db:"balanceAfter"`
	Activity      string `json:"activity" db:"activity"`
	Type          string `json:"type" db:"type" validate:"required" oneof:"kredit debit"`
	IP            string `json:"ip" db:"ip"`
	Location      string `json:"location" db:"location"`
	UserAgent     string `json:"userAgent" db:"userAgent"`
	Author        string `json:"author" db:"author"`
}

type TopUp struct {
	Balance int `json:"balance"`
	// Type string `json:"type"`
}

type Transfer struct {
	UserID  int `json:"userid"`
	Balance int `json:"balance"`
	// Type string `json:"type"`
}
