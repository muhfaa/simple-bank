package spec

import (
	"errors"
	"net/http"
)

var (
	// ==================== From Resource ====================

	// ErrUserrNotFound : resource is not found
	ErrUserrNotFound = errors.New("User was not found")

	// ErrUserBalanceNotFound : resource is not found
	ErrUserBalanceNotFound = errors.New("User balance not found")

	// ErrUserBalanceHistoryNotFound : resource is not found
	ErrUserBalanceHistoryNotFound = errors.New("User balance history not found")

	// ErrBankBalanceNotFound : resource is not found
	ErrBankBalanceNotFound = errors.New("Bank balance not found")

	// ErrBankBalanceHistoryNotFound : resource is not found
	ErrBankBalanceHistoryNotFound = errors.New("Bank balance history not found")

	// ErrTopUpFail : error message when topup is invalid
	ErrTopUpFail = errors.New("Topup Fail")

	// ErrInvalidUsernamePassword : error message when login information is invalid
	ErrInvalidUsernamePassword = errors.New("Invalid username or password")

	// ErrResource : resource error cause something
	ErrResource = errors.New("Resource error")

	// ErrTransactionFail : resource error cause something
	ErrTransactionFail = errors.New("Transaction Fail")

	// ErrBalanceNotEnough : resource error cause something
	ErrBalanceNotEnough = errors.New("Your balance is not enough")

	// ErrDuplicateEntryEmailOrUsername : resource error, email or username should be unique
	ErrDuplicateEntryEmailOrUsername = errors.New("Email or username are already registered")

	// ==================== From Business ====================

	// ErrInvalidSpec : invalid spec
	ErrInvalidSpec = errors.New("Given spec is not valid")
)

// ErrToHTTPCode : translate error to http code
func ErrToHTTPCode(err error) (httpCode int) {
	switch err {
	case ErrUserrNotFound, ErrUserBalanceNotFound, ErrUserBalanceHistoryNotFound, ErrBankBalanceNotFound, ErrBankBalanceHistoryNotFound:
		httpCode = http.StatusNotFound
	case ErrInvalidSpec, ErrDuplicateEntryEmailOrUsername, ErrTopUpFail, ErrTransactionFail, ErrBalanceNotEnough:
		httpCode = http.StatusUnprocessableEntity
	case ErrInvalidUsernamePassword:
		httpCode = http.StatusUnauthorized
	default:
		httpCode = http.StatusInternalServerError
	}
	return
}
