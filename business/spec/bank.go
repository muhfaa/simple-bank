package spec

// BankBalance ...
type BankBalance struct {
	ID             int    `json:"_" db:"id"`
	Balance        int    `json:"balance" db:"balance"`
	BalanceAchieve int    `json:"balanceAchieve" db:"balanceAchieve"`
	Code           string `json:"code" db:"code"`
	Enable         bool   `json:"enable" db:"enable"`
	UserBalanceID  int    `json:"userBalanceID" db:"userBalanceID"`
}

// BankBalanceHistory ...
type BankBalanceHistory struct {
	ID            int    `json:"_" db:"id"`
	BankBalanceID int    `json:"bankBalanceID" db:"bankBalanceID"`
	BalanceBefore int    `json:"balanceBefore" db:"balanceBefore"`
	BalanceAfter  int    `json:"balanceAfter" db:"balanceAfter"`
	Activity      string `json:"activity" db:"activity"`
	Type          string `json:"type" db:"type"`
	IP            string `json:"ip" db:"ip"`
	Location      string `json:"location" db:"location"`
	UserAgent     string `json:"userAgent" db:"userAgent"`
	Author        string `json:"author" db:"author"`
}
