CREATE TABLE IF NOT EXISTS `user` (
	id int(11) NOT NULL AUTO_INCREMENT,
	username varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	password varchar(1000) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `user_balance` (
	id int NOT NULL AUTO_INCREMENT,
	userid int NOT NULL,
	balance int NOT NULL DEFAULT 0,
	balanceAchieve int NOT NULL DEFAULT 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `user_balance_history` (
	id int NOT NULL AUTO_INCREMENT,
	userBalanceID int NOT NULL,
	balanceBefore int NOT NULL DEFAULT 0,
	balanceAfter int NOT NULL DEFAULT 0,
	activity varchar(100),
	type enum('kredit', 'debit'),
	ip varchar(100),
	location varchar(100),
	userAgent varchar(100),
	author varchar(100),
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `bank_balance` (
	id int NOT NULL AUTO_INCREMENT,
	balance int NOT NULL DEFAULT 0,
	balanceAchieve int NOT NULL DEFAULT 0,
	code varchar(100),
	enable BOOLEAN,
	userBalanceID int,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `bank_balance_history` (
	id int NOT NULL AUTO_INCREMENT,
	bankBalanceID int NOT NULL,
	balanceBefore int NOT NULL DEFAULT 0,
	balanceAfter int NOT NULL DEFAULT 0,
	activity varchar(100),
	type enum('kredit', 'debit'),
	ip varchar(100),
	location varchar(100),
	userAgent varchar(100),
	author varchar(100),
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `user_balance`(userid, balance, balanceAchieve) VALUE (1, 1000000, 1000000), (2, 2000000, 2000000), (3, 3000000, 3000000)

INSERT INTO `user_balance_history` (
		userBalanceID,
		balanceBefore,
		balanceAfter,
		activity,
		type,
		ip,
		location,
		userAgent,
		author
	)
	VALUE
	(1, 0, 1000000, "TopUp", "debit", "0.0.0.0", "indonesia", "aldi", "aldi"),
    (2, 0, 2000000, "TopUp", "debit", "0.0.0.0", "indonesia", "akbar", "akbar"),
    (3, 0, 3000000, "TopUp", "debit", "0.0.0.0", "indonesia", "lazuardi", "lazuardi")

INSERT INTO `bank_balance`(balance, balanceAchieve, code, enable, ) VALUE (1, 1000000, "001", true), (2, 2000000, "002", true), (3, 3000000, "003", true)

INSERT INTO `bank_balance_history` (
		bankBalanceID,
		balanceBefore,
		balanceAfter,
		activity,
		type,
		ip,
		location,
		userAgent,
		author
	)
	VALUE
    (1, 0, 1000000, "TopUp", "debit", "0.0.0.0", "indonesia", "aldi", "aldi"),
    (2, 0, 2000000, "TopUp", "debit", "0.0.0.0", "indonesia", "akbar", "akbar"),
    (3, 0, 3000000, "TopUp", "debit", "0.0.0.0", "indonesia", "lazuardi", "lazuardi")
